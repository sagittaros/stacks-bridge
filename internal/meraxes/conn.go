package meraxes

import "database/sql"

// Conn holds connection used by different packages
var Conn *sql.DB
