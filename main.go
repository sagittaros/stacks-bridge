package main

import (
	"flag"

	"gitlab.com/sagittaros/pqconn"
	"gitlab.com/sagittaros/stacks-bridge/internal/meraxes"
	"gitlab.com/sagittaros/stacks-bridge/internal/stacks"
)

func main() {
	meraxesConnStr := flag.String("meraxes", "", "Meraxes PG connection string")
	stacksConnStr := flag.String("stacks", "", "Stacks PG connection string")

	flag.Parse()
	meraxes.Conn = pqconn.EstablishConnection(*meraxesConnStr)
	stacks.Conn = pqconn.EstablishConnection(*stacksConnStr)
	defer meraxes.Conn.Close()
	defer stacks.Conn.Close()

}
